package me.zearthur.ed2bench;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.ordering.*;
import me.zearthur.ed2bench.time.TimeManager;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> lengths = new ArrayList();
        lengths.add(1000);
        lengths.add(5000);
        lengths.add(10000);
        lengths.add(50000);
        lengths.add(100000);
        lengths.add(500000);
        lengths.add(1000000);

        lengths.add(5000000);
        lengths.add(10000000);
        lengths.add(50000000);
        lengths.add(100000000);
        lengths.add(500000000);
        lengths.add(1000000000);

        ArrayList<OrderingMethod> tests = new ArrayList();

        tests.add(new ArraySort());
        tests.add(new BubbleSort());
        tests.add(new DirectInsertion());
        tests.add(new DirectSelection());
        tests.add(new HeapSort());
        tests.add(new MergeSort());
        tests.add(new QuickSort());
        tests.add(new ShellSort());

        TimeManager tm = TimeManager.getInstance();
        VectorFactory vf = new VectorFactory();

        for (OrderingMethod orderingMethod : tests){

            for(Integer length : lengths){
                System.out.println(tm.getCurrentStamp() + "[*] Testando tamanho de vetor " + length);
                int[] randomVector = vf.getRandomVector(length);
                int[] orderedVector = vf.getOrderedVector(length);
                int[] inverseVector = vf.getInverseOrderedVector(length);

                System.out.println(tm.getCurrentStamp() + "  [*] Vetores gerados, iniciando testes");

                System.out.println(tm.getCurrentStamp() + "  [x] Testando método de ordenação " + orderingMethod.getName() + " com vetor de tamanho " + length);
                boolean shouldBreak = false;
                tm.startTimer();
                try {
                    orderingMethod.orderVector(randomVector);
                } catch (TimeoutException err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Tempo limite esgotado para o método " + orderingMethod.getName() + " com vetor aleatório de tamanho " + length);
                    shouldBreak = true;
                }catch (StackOverflowError err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Pilha máxima para o método " + orderingMethod.getName() + " com vetor aleatório de tamanho " + length);
                    shouldBreak = true;
                }
                tm.stopTimer();
                System.out.println(tm.getCurrentStamp() + "    [*] " + orderingMethod.getName() + " finalizou o teste de vetor aleatório de tamanho " + length + " em " + tm.getTotalTimeDiff() + "ms");
                tm.reset();

                tm.startTimer();
                try {
                    orderingMethod.orderVector(orderedVector);
                } catch (TimeoutException err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Tempo limite esgotado para o método " + orderingMethod.getName() + " com vetor ordenado de tamanho " + length);
                    shouldBreak = true;
                }catch (StackOverflowError err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Pilha máxima para o método " + orderingMethod.getName() + " com vetor aleatório de tamanho " + length);
                    shouldBreak = true;
                }
                tm.stopTimer();
                System.out.println(tm.getCurrentStamp() + "    [*] " + orderingMethod.getName() + " finalizou o teste de vetor ordenado de tamanho " + length + " em " + tm.getTotalTimeDiff() + "ms");
                tm.reset();

                tm.startTimer();
                try {
                    orderingMethod.orderVector(inverseVector);
                } catch (TimeoutException err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Tempo limite esgotado para o método " + orderingMethod.getName() + " com vetor inversamente ordenado de tamanho " + length);
                    shouldBreak = true;
                }catch (StackOverflowError err){
                    System.out.println(tm.getCurrentStamp() + "    [*] Pilha máxima para o método " + orderingMethod.getName() + " com vetor aleatório de tamanho " + length);
                    shouldBreak = true;
                }
                tm.stopTimer();
                System.out.println(tm.getCurrentStamp() + "    [*] " + orderingMethod.getName() + " finalizou o teste de vetor inversamente ordenado de tamanho " + length + " em " + tm.getTotalTimeDiff() + "ms");
                tm.reset();

                if(shouldBreak) { break; }
            }

        }

        tm.stopTimer();

    }

}
