package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 09/11/18.
 */
public class DirectSelection implements OrderingMethod {
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        int i, j, menor;
        int aux;
        int fim = vector.length - 1;
        for (i = 0; i < fim; i++)  {
            menor = i;
            for (j = i + 1; j <= fim ; j++) {
                TimeManager.getInstance().checkOperationTimeout();
                if (vector[j] < vector[menor]) {
                    menor = j;
                }
            }
            aux = vector[i];
            vector[i] = vector[menor];
            vector[menor] = aux;
        }
        return vector;
    }

    @Override
    public String getName() {
        return "Direct Selection";
    }
}
