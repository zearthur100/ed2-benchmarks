package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;

/**
 * Created by zearthur99 on 02/11/18.
 */
public interface OrderingMethod {

    public int[] orderVector(int[] vector) throws TimeoutException;
    public String getName();

}
