package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class ShellSort implements OrderingMethod {
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        int i , j , h = 1, value ;
        do {
            h = 3 * h + 1;
        } while ( h < vector.length );
        do {
            h = h / 3;
            for ( i = h; i < vector.length; i++) {
                value = vector [ i ];
                j = i - h;
                while (j >= 0 && value < vector [ j ]) {
                    vector [ j + h ] = vector [ j ];
                    j = j - h;
                }
                vector[ j + h ] = value;
            }
            TimeManager.getInstance().checkOperationTimeout();
        } while ( h > 1 );
        return vector;
    }

    @Override
    public String getName() {
        return "Shellsort";
    }
}
