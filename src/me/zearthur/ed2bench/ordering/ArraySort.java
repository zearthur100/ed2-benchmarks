package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

import java.util.Arrays;

/**
 * Created by zearthur99 on 09/11/18.
 */
public class ArraySort implements OrderingMethod{
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        Arrays.sort(vector);
        return vector;
    }

    @Override
    public String getName() {
        return "Java Default";
    }
}
