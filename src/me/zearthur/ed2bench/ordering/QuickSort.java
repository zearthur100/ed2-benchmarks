package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class QuickSort implements OrderingMethod{
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        quickSort(vector,0 , vector.length - 1);
        return vector;
    }

    @Override
    public String getName() {
        return "Quicksort";
    }

    private static void quickSort(int[] vetor, int inicio, int fim) throws TimeoutException {
        TimeManager.getInstance().checkOperationTimeout();
        if (inicio < fim) {
            int posicaoPivo = separar(vetor, inicio, fim);
            quickSort(vetor, inicio, posicaoPivo - 1);
            quickSort(vetor, posicaoPivo + 1, fim);
        }
    }

    private static int separar(int[] vetor, int inicio, int fim) {
        int pivo = vetor[inicio];
        int i = inicio + 1, f = fim;
        while (i <= f) {
            if (vetor[i] <= pivo)
                i++;
            else if (pivo < vetor[f])
                f--;
            else {
                int troca = vetor[i];
                vetor[i] = vetor[f];
                vetor[f] = troca;
                i++;
                f--;
            }
        }
        vetor[inicio] = vetor[f];
        vetor[f] = pivo;
        return f;
    }
}
