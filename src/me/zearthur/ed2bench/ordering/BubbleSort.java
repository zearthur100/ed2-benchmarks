package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 09/11/18.
 */
public class BubbleSort implements OrderingMethod{
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        int fim = vector.length - 2;
        int pos = 0;
        boolean troca = true;
        int chave;
        int i;
        while (troca) {
            troca = false;
            TimeManager.getInstance().checkOperationTimeout();
            for (i = 0; i <= fim; i++) {
                if (vector[i] > vector[i+1]) {
                    chave = vector[i];
                    vector[i] = vector[i+1];
                    vector[i+1] = chave;
                    pos = i;
                    troca = true;
                }
            }
            fim = pos-1;
        }
        return vector;
    }

    @Override
    public String getName() {
        return "Bubble sort";
    }
}
