package me.zearthur.ed2bench.time;

import me.zearthur.ed2bench.exception.TimeoutException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class TimeManager {

    private static TimeManager instance = null;

    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public static TimeManager getInstance(){
        if(instance == null){
            instance = new TimeManager();
        }
        return instance;
    }

    private TimeManager() {
        this.reset();
    }

    public void startTimer(){
        startTime = LocalDateTime.now();
    }

    public void stopTimer() {
        endTime = LocalDateTime.now();
    }

    public long getTotalTimeDiff(){
        Duration d = Duration.between(startTime, endTime);
        return d.toMillis();
    }

    public long getCurrentTimeDiff(){
        Duration d = Duration.between(startTime, LocalDateTime.now());
        return d.toMillis();
    }

    public void reset(){
        startTime = null;
        endTime = null;
    }

    public void checkOperationTimeout() throws TimeoutException {
        if (getCurrentTimeDiff() >= 3600000) {
            throw new TimeoutException();
        }
    }

    public String getCurrentStamp(){
        LocalDateTime now = LocalDateTime.now();
        return now.getDayOfMonth() + "/" + now.getMonthValue() + "/" + now.getYear() + " " + now.getHour() + ":" + now.getMinute() + ":" + now.getSecond() + " ";
    }

}
