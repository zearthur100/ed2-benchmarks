package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 09/11/18.
 */
public class DirectInsertion implements OrderingMethod{
    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {

        int i, j;
        int chave;
        int fim = vector.length - 1;
        for (i = 1; i <= fim; i++) {
            chave = vector[i];
            j = i - 1;
            while (j >= 0 && vector[j] > chave) {
                TimeManager.getInstance().checkOperationTimeout();
                vector[j+1] = vector[j];
                j = j - 1;
            }
            vector[j+1] = chave;
        }

        return vector;
    }

    @Override
    public String getName() {
        return "Direct insertion";
    }
}
