package me.zearthur.ed2bench;

import java.util.Random;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class VectorFactory {

    public int[] getRandomVector(int size) {
        int result[] = new int[size];
        Random r = new Random();

        for(int i = 0; i < result.length; i++) {
            if(i % 10000000 == 0){
                //System.out.println("Generating index " + i + "/" + size);
            }

            result[i] = r.nextInt((10000 - 0) + 1) + 0;

            //result[i] = 5;
        }
        //System.out.println("Vector generated");
        return result;
    }

    public int [] getOrderedVector(int size) {
        int[] v = new int[size];
        for(int i = 1; i < v.length; i++){
            v[i] = i;
        }
        return v;
    }

    public int[] getInverseOrderedVector(int size) {
        int[] v = new int[size];
        for(int i = v.length - 1; i > -1; i--){
            v[i] = v.length - i - 1;
        }
        return v;
    }



}
