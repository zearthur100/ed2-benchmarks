package me.zearthur.ed2bench.ordering;

import me.zearthur.ed2bench.exception.TimeoutException;
import me.zearthur.ed2bench.time.TimeManager;

/**
 * Created by zearthur99 on 02/11/18.
 */
public class MergeSort implements OrderingMethod{
    public static int[] mergeSort(int v[], int inicio, int fim) throws TimeoutException {
        int meio;

        TimeManager.getInstance().checkOperationTimeout();


        if (inicio == fim) { // tamanho = 1
            return v;
        } else {
            // encontra o meio
            meio = (inicio + fim) / 2;
            // ordena 1a metade
            mergeSort(v, inicio, meio);
            mergeSort(v, meio + 1, fim);
            // intercala
            merge(v, inicio, meio, fim);
        }
        return v;
    }

    public static void merge(int vetor[], int inicio, int meio, int fim) {
        int prim = inicio;
        int seg = meio + 1;
        int i = inicio;
        int[] aux = new int[fim+1];
        // enquanto tiver elementos nos dois conjuntos
        while ((prim <= meio) && (seg <= fim)) {
            // insere elemento do primeiro
            if (vetor[prim] <= vetor[seg]) {
                aux[i] = vetor[prim];
                prim++;
            } else { // insere elemento do segundo
                aux[i] = vetor[seg];
                seg++;
            }
            i++;
        }
        // sobrou elementos do segundo
        if (prim > meio) {
            while (seg <= fim) {
                aux[i] = vetor[seg];
                seg++;
                i++;
            }
            // sobrou elementos do primeiro
        } else {
            while (prim <= meio) {
                aux[i] = vetor[prim];
                prim++;
                i++;
            }
        }
        // copia o vetor auxiliar no vetor principal
        for (i = inicio; i <= fim; i++) {
            vetor[i] = aux[i];
        }
    }

    @Override
    public int[] orderVector(int[] vector) throws TimeoutException {
        return mergeSort(vector, 0, vector.length - 1);
    }

    @Override
    public String getName() {
        return "Merge sort";
    }
}